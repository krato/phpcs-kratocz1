<?php

namespace phpcs;

class Good
{

	const IS_IN = ' is in ';
	
	/** @var string Good variable */
	public $goodVariable = 'Goodness';

	public function someMethod($text)
	{
		if (true || true)
		{
			return $this->goodVariable . self::IS_IN . $text;
		}
	}

}
