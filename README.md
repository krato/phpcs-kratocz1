# README.md #

## Description ##

PHP code style standard by Petr Kratochvil (krato@krato.cz).

It is based on PSR-2 but it has some little differences. It extends PSR-2 code style definion rules for phpcs:
https://github.com/squizlabs/PHP_CodeSniffer/blob/master/CodeSniffer/Standards/PSR2/ruleset.xml

## Files ##

* **phpcs.xml** ... PHP code style definition for PHP Code Sniffer (phpcs)
* good.php ... example of well formatted PHP code
* bad.php ... example of bad formatted PHP code
* example-output.php ... example of phpcs output after checking this project

## Requirements ##

* PHP 5₊
* phpcs

## Usage ##

1. **Copy phpcs.xml** to root directory of your project.
2. *Optional:* Adjust phpcs.xml to your needs, if you want. You can paste the file content to this usefull generator and adjust it: http://edorian.github.io/php-coding-standard-generator/#phpcs
3. *Optional:* Adjust directory path to check, if needed. It is line "<file>./</file>" in the phpcs.xml . For example if you need to recursively check directory "app" only: <file>./app</file>
4. **Run phpcs** in root directory of your project.
5. *Optional:* Run phpcbf to auto-fix the source code. This will modify your source files.